# TensorFlow2 EfficientNet
An EfficientNetB0 is trained on BigEarthNet-19, using 12 bands. Multi-label classification problem (thus using sigmoid as last layer and binary crossentropy as loss function).
The dataset is saved as multiple tfrecord files for training and validation. Test script not included in this repository as it is a simple inference on 1 GPU and of interest.
Horovod is employed to scale the training on multiple GPUs.


## Requirements
+ Python >= 3.6
+ Tensorflow >= 2.0.0 (currently using 2.3.1)
+ Horovod (currently using 0.20.3)
+ Scipy (currently using 1.5.2)

## Data
+ Data is available at: https://drive.google.com/drive/folders/1Km5roroaYS4HNholuhJ3QpQVNArpNYHx?usp=sharing
+ Unzip the dataset and place at your preferred data location
+ Data are parsed, normalized and upsampled in **prepare_data_mixup.py**
+ in **normalization_array** the numpy file for normalization of the data is stored
+ Modify **config.py** adding the correct path of the data

## Training
+ Run **train_hvd_keras_aug_effnet.py** to start training, training and validation scores and losses visible in the output file
+ **execute_tensorboard_juwels.sh** is an example job script to run the training on JUWELS

## Contact info
For further informations, contact me at r.sedona@fz-juelich.de
