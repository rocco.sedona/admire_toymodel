#!/usr/bin/env bash

# Slurm job configuration
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-gpu=20
#SBATCH --account=training
#SBATCH --output=output/out_effnet_2nodes.out
#SBATCH --error=output/out_effnet_2nodes.er
#SBATCH --time=2:00:00
#SBATCH --job-name=BENTF2EFFNET
#SBATCH --gres=gpu:1 --partition=develgpus

#load modules
ml Stages/2020  GCC/9.3.0  OpenMPI/4.1.0rc1
ml Horovod/0.20.3-Python-3.8.5
ml TensorFlow/2.3.1-Python-3.8.5

export CUDA_VISIBLE_DEVICES="0,1,2,3"

#run Python program
srun --cpu-bind=none python -u train_hvd_keras_aug_effnet.py
