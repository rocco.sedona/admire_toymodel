from __future__ import absolute_import, division, print_function
import tensorflow as tf
import config
from prepare_data_mixup import generate_datasets, flip, color, zoom, rotate, mixup
import math
import datetime
import horovod.tensorflow.keras as hvd

hvd.init()

if __name__ == '__main__':

    # Pin GPU to be used to process local rank (one GPU per process)
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
    if gpus:
        tf.config.experimental.set_visible_devices(
            gpus[hvd.local_rank()], 'GPU')

    # create model
    model = tf.keras.applications.EfficientNetB0(
    include_top=True, weights=None, input_tensor=None,
    input_shape=(config.image_height, config.image_width, config.channels), pooling=None, classes=config.NUM_CLASSES,
    classifier_activation='sigmoid')

    if hvd.rank() == 0:
        model.summary()

    # get the original_dataset
    BATCH_SIZE = config.BATCH_SIZE
    train_dataset, valid_dataset, test_dataset, train_count, valid_count, test_count = generate_datasets(
        hvd.size(), hvd.rank())
    train_dataset = train_dataset.repeat().shuffle(10000, reshuffle_each_iteration=False)

    # Make sure that the values are still in [0, 1]
    #train_dataset = train_dataset.map(lambda x, y: tf.clip_by_value(x, 0, 1), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    #train_dataset = train_dataset.cache().batch(batch_size=BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)
    #train_dataset = train_dataset.batch(batch_size=BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)
    train_dataset = train_dataset.batch(batch_size=BATCH_SIZE)
    # Add augmentations
    if config.AUGM == True:
       augmentations = [flip, rotate, mixup]
       # Add the augmentations to the dataset
       for f in augmentations:
          # Apply the augmentation, run 4 jobs in parallel.
          train_dataset = train_dataset.map(f, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    train_dataset = train_dataset.prefetch(tf.data.experimental.AUTOTUNE)
    #valid_dataset = valid_dataset.map(lambda x, y: tf.clip_by_value(x, 0, 1), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    #valid_dataset = valid_dataset.cache().batch(batch_size=BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)
    valid_dataset = valid_dataset.batch(batch_size=BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)
    #test_dataset = test_dataset.map(lambda x, y: tf.clip_by_value(x, 0, 1), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    #test_dataset = test_dataset.batch(batch_size=BATCH_SIZE)
    
    print("finished initial data processing")
    print("hvd rank: ", hvd.rank(), ", hvd size:", hvd.size())

    # Horovod: adjust learning rate based on number of GPUs.
    starter_learning_rate = 0.003
    end_learning_rate = 0.0001
    decay_steps = 10000
    scaled_lr = tf.keras.optimizers.schedules.PolynomialDecay(
       starter_learning_rate,
       decay_steps,
       end_learning_rate,
       power=1)
    
    scaled_lr = 0.001 * hvd.size()
    opt = tf.optimizers.Adam(scaled_lr)
    opt = hvd.DistributedOptimizer(opt)

    # Horovod: Specify `experimental_run_tf_function=False` to ensure TensorFlow
    # uses hvd.DistributedOptimizer() to compute gradients.
    print("before compiling the model")
    model.compile(loss=tf.keras.losses.BinaryCrossentropy(),
                  optimizer=opt,
                  metrics=[tf.keras.metrics.Precision(
                      thresholds=0.3, name='train_precision'), tf.keras.metrics.Recall(
                      thresholds=0.3, name='train_recall')],
                  experimental_run_tf_function=False)

    callbacks = [
        # Horovod: broadcast initial variable states from rank 0 to all other processes.
        # This is necessary to ensure consistent initialization of all workers when
        # training is started with random weights or restored from a checkpoint.
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
        # Horovod: average metrics among workers at the end of every epoch.
        #
        # Note: This callback must be in the list before the ReduceLROnPlateau,
        # TensorBoard or other metrics-based callbacks.
        hvd.callbacks.MetricAverageCallback(),
    ]

    # Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
    if hvd.rank() == 0:
        callbacks.append(tf.keras.callbacks.ModelCheckpoint(
            './checkpoints_200ep/checkpoint-{epoch}.h5', save_freq="epoch", period = 10))

    # Horovod: using `lr = 1.0 * hvd.size()` from the very beginning leads to worse final
    # accuracy. Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` during
    # the first three epochs. See https://arxiv.org/abs/1706.02677 for details.
    """
    hvd.callbacks.LearningRateWarmupCallback(
       initial_lr=starter_learning_rate, warmup_epochs=10, verbose=1),
    """

    # Horovod: write logs on worker 0.
    verbose = 1 if hvd.rank() == 0 else 0
    print("before fitting")
    # Train the model.
    # Horovod: adjust number of steps based on number of GPUs.
    steps_per_epoch = train_count // (config.BATCH_SIZE * hvd.size())
    model.fit(train_dataset, steps_per_epoch=steps_per_epoch,
              callbacks=callbacks, epochs=config.EPOCHS, validation_data = valid_dataset, verbose=verbose)
