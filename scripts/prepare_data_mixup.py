import tensorflow as tf
import config
import pathlib
from config import image_height, image_width, channels
import numpy as np
from scipy import ndimage
import glob

#def generate_datasets():
def generate_datasets(hvd_size, hvd_rank):
    image_feature_description = {
       'B01': tf.io.FixedLenFeature([400], dtype=tf.int64),
       'B02': tf.io.FixedLenFeature([14400], dtype=tf.int64),
       'B03': tf.io.FixedLenFeature([14400], dtype=tf.int64),
       'B04': tf.io.FixedLenFeature([14400], dtype=tf.int64),
       'B05': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       'B06': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       'B07': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       'B08': tf.io.FixedLenFeature([14400], dtype=tf.int64),
       'B8A': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       'B09': tf.io.FixedLenFeature([400], dtype=tf.int64),
       'B11': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       'B12': tf.io.FixedLenFeature([3600], dtype=tf.int64),
       #'BigEarthNet-19_labels': tf.io.VarLenFeature(tf.string),
       'BigEarthNet-19_labels_multi_hot': tf.io.VarLenFeature(tf.int64),
       #'patch_name': tf.io.FixedLenFeature([], dtype=tf.string),
       }


    def _parse_image_function(example_proto):
      # Parse the input tf.Example proto using the dictionary above
      parsed_example = tf.io.parse_single_example(example_proto, image_feature_description)
      #val_rescale = 0.001
      #b_r = tf.dtypes.cast(tf.reshape(parsed_example['B02'], [120,120]), tf.float32)*val_rescale
      #b_g = tf.dtypes.cast(tf.reshape(parsed_example['B03'], [120,120]), tf.float32)*val_rescale
      #b_b = tf.dtypes.cast(tf.reshape(parsed_example['B04'], [120,120]), tf.float32)*val_rescale
      #normalize (subtract max and divide by std)
      b_r = tf.dtypes.cast((tf.reshape(parsed_example['B02'], [120,120])-arr_norm[2, 0])/arr_norm[3, 0], tf.float32)
      b_g = tf.dtypes.cast((tf.reshape(parsed_example['B03'], [120,120])-arr_norm[2, 1])/arr_norm[3, 1], tf.float32)
      b_b = tf.dtypes.cast((tf.reshape(parsed_example['B04'], [120,120])-arr_norm[2, 2])/arr_norm[3, 2], tf.float32)
      b_8 = tf.dtypes.cast((tf.reshape(parsed_example['B08'], [120,120])-arr_norm[2, 3])/arr_norm[3, 3], tf.float32)
      stacked_bands_10m = tf.stack([b_r, b_g, b_b, b_8], axis=2)

      b_5 = tf.dtypes.cast((tf.reshape(parsed_example['B05'], [60,60])-arr_norm[2, 4])/arr_norm[3, 4], tf.float32)
      #b_5 = tf.image.resize(b_5, [120,120])
      b_6 = tf.dtypes.cast((tf.reshape(parsed_example['B06'], [60,60])-arr_norm[2, 5])/arr_norm[3, 5], tf.float32)
      #b_6 = tf.image.resize(b_6, [120,120])
      b_7 = tf.dtypes.cast((tf.reshape(parsed_example['B07'], [60,60])-arr_norm[2, 6])/arr_norm[3, 6], tf.float32)
      #b_7 = tf.image.resize(b_7, [120,120])
      b_8a = tf.dtypes.cast((tf.reshape(parsed_example['B8A'], [60,60])-arr_norm[2, 7])/arr_norm[3, 7], tf.float32)
      #b_8a = tf.image.resize(b_8a, [120,120])
      b_11 = tf.dtypes.cast((tf.reshape(parsed_example['B11'], [60,60])-arr_norm[2, 8])/arr_norm[3, 8], tf.float32)
      #b_11 = tf.image.resize(b_11, [120,120])
      b_12 = tf.dtypes.cast((tf.reshape(parsed_example['B12'], [60,60])-arr_norm[2, 9])/arr_norm[3, 9], tf.float32)
      #b_12 = tf.image.resize(b_12, [120,120])
      stacked_bands_20m = tf.stack([b_5, b_6, b_7, b_8a, b_11, b_12], axis=2)
      stacked_bands_20m = tf.image.resize(stacked_bands_20m, [120,120])

      b_1 = tf.dtypes.cast((tf.reshape(parsed_example['B01'], [20,20])-arr_norm[2, 10])/arr_norm[3, 10], tf.float32)
      #b_1 = tf.image.resize(b_1, [120,120])
      b_9 = tf.dtypes.cast((tf.reshape(parsed_example['B09'], [20,20])-arr_norm[2, 11])/arr_norm[3, 11], tf.float32)
      #b_9 = tf.image.resize(b_9, [120,120])
      stacked_bands_60m = tf.stack([b_1, b_9], axis=2)
      stacked_bands_60m = tf.image.resize(stacked_bands_60m, [120,120])
      stacked_bands = tf.concat([stacked_bands_10m, stacked_bands_20m, stacked_bands_60m], axis=2)
      #stacked_bands = tf.dtypes.cast(stacked_bands, tf.float32)
      labels =  tf.dtypes.cast(tf.reshape(tf.sparse.to_dense(parsed_example['BigEarthNet-19_labels_multi_hot']), [19]), tf.float32)
      #labels =  parsed_example['BigEarthNet-19_labels_multi_hot']

      return stacked_bands, labels

    def _parse_image_function_6cha(example_proto):
      # Parse the input tf.Example proto using the dictionary above
      parsed_example = tf.io.parse_single_example(example_proto, image_feature_description)
      #normalize (subtract max and divide by std)
      b_r = tf.dtypes.cast((tf.reshape(parsed_example['B02'], [120,120])-arr_norm[2, 0])/arr_norm[3, 0], tf.float32)
      b_g = tf.dtypes.cast((tf.reshape(parsed_example['B03'], [120,120])-arr_norm[2, 1])/arr_norm[3, 1], tf.float32)
      b_b = tf.dtypes.cast((tf.reshape(parsed_example['B04'], [120,120])-arr_norm[2, 2])/arr_norm[3, 2], tf.float32)
      b_8 = tf.dtypes.cast((tf.reshape(parsed_example['B08'], [120,120])-arr_norm[2, 3])/arr_norm[3, 3], tf.float32)
      stacked_bands_10m = tf.stack([b_r, b_g, b_b, b_8], axis=2)

      b_11 = tf.dtypes.cast((tf.reshape(parsed_example['B11'], [60,60])-arr_norm[2, 8])/arr_norm[3, 8], tf.float32)
      #b_11 = tf.image.resize(b_11, [120,120])
      b_12 = tf.dtypes.cast((tf.reshape(parsed_example['B12'], [60,60])-arr_norm[2, 9])/arr_norm[3, 9], tf.float32)
      #b_12 = tf.image.resize(b_12, [120,120])
      stacked_bands_20m = tf.stack([b_11, b_12], axis=2)
      stacked_bands_20m = tf.image.resize(stacked_bands_20m, [120,120])

      stacked_bands = tf.concat([stacked_bands_10m, stacked_bands_20m], axis=2)
      #stacked_bands = tf.dtypes.cast(stacked_bands, tf.float32)
      labels =  tf.dtypes.cast(tf.reshape(tf.sparse.to_dense(parsed_example['BigEarthNet-19_labels_multi_hot']), [19]), tf.float32)
      #labels =  parsed_example['BigEarthNet-19_labels_multi_hot']
      return stacked_bands, labels
    #load array with min, max and std to normalize each channel
    arr_norm = np.load("normalization_array/arr_minmax.npy")
    #train_dataset = tf.data.TFRecordDataset(config.train_dir)
    train_dataset = tf.data.TFRecordDataset(glob.glob(config.train_dir + "*.tfrecord")).shard(hvd_size, hvd_rank)
    train_dataset = train_dataset.map(_parse_image_function, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    train_count = sum(1 for _ in tf.data.TFRecordDataset(glob.glob(config.train_dir + "*.tfrecord")))
    #valid_dataset = tf.data.TFRecordDataset(config.valid_dir)
    valid_dataset = tf.data.TFRecordDataset(glob.glob(config.valid_dir + "*.tfrecord")).shard(hvd_size, hvd_rank)
    valid_dataset = valid_dataset.map(_parse_image_function, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    valid_count = sum(1 for _ in tf.data.TFRecordDataset(glob.glob(config.train_dir + "*.tfrecord")))
    test_dataset = tf.data.TFRecordDataset(config.test_dir)
    test_dataset = test_dataset.map(_parse_image_function, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    test_count = sum(1 for _ in tf.data.TFRecordDataset(config.test_dir))

    return train_dataset, valid_dataset, test_dataset, train_count, valid_count, test_count
"""
def cutmix(image, label, PROBABILITY = 1.0):
    # input image - is a batch of images of size [n,dim,dim,nchannels] not a single image of [dim,dim,nchannels]
    # output - a batch of images with cutmix applied
    DIM = 120
    CLASSES = 19
    AUG_BATCH = config.BATCH_SIZE
    N_CHANNELS = 12
    imgs = []; labs = []
    for j in range(AUG_BATCH):
        # DO CUTMIX WITH PROBABILITY DEFINED ABOVE
        P = tf.cast( tf.random.uniform([],0,1)<=PROBABILITY, tf.int32)
        # CHOOSE RANDOM IMAGE TO CUTMIX WITH
        k = tf.cast( tf.random.uniform([],0,AUG_BATCH),tf.int32)
        # CHOOSE RANDOM LOCATION
        x = tf.cast( tf.random.uniform([],0,DIM),tf.int32)
        y = tf.cast( tf.random.uniform([],0,DIM),tf.int32)
        b = tf.random.uniform([],0,1) # this is beta dist with alpha=1.0
        WIDTH = tf.cast( DIM * tf.math.sqrt(1-b),tf.int32) * P
        ya = tf.math.maximum(0,y-WIDTH//2)
        yb = tf.math.minimum(DIM,y+WIDTH//2)
        xa = tf.math.maximum(0,x-WIDTH//2)
        xb = tf.math.minimum(DIM,x+WIDTH//2)
        # MAKE CUTMIX IMAGE
        one = image[j,ya:yb,0:xa,:]
        two = image[k,ya:yb,xa:xb,:]
        three = image[j,ya:yb,xb:DIM,:]
        middle = tf.concat([one,two,three],axis=1)
        img = tf.concat([image[j,0:ya,:,:],middle,image[j,yb:DIM,:,:]],axis=0)
        imgs.append(img)
        # MAKE CUTMIX LABEL
        #a = tf.cast(WIDTH*WIDTH/DIM/DIM,tf.float32)
        a = tf.cast( (xb-xa) * (yb-ya) / DIM / DIM,tf.float32)
        if len(label.shape)==1:
            lab1 = tf.one_hot(label[j],CLASSES)
            lab2 = tf.one_hot(label[k],CLASSES)
        else:
            lab1 = label[j,]
            lab2 = label[k,]
        labs.append((1-a)*lab1 + a*lab2)
            
    # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
    image2 = tf.reshape(tf.stack(imgs),(AUG_BATCH,DIM,DIM,N_CHANNELS))
    label2 = tf.reshape(tf.stack(labs),(AUG_BATCH,CLASSES))
    return image2,label2
"""


def mixup(image, label, PROBABILITY = 1.0):
    # input image - is a batch of images of size [n,dim,dim,3] not a single image of [dim,dim,3]
    # output - a batch of images with mixup applied
    DIM = 120
    CLASSES = 19
    AUG_BATCH = config.BATCH_SIZE
    N_CHANNELS = 12
    
    imgs = []; labs = []
    for j in range(AUG_BATCH):
        # DO MIXUP WITH PROBABILITY DEFINED ABOVE
        P = tf.cast( tf.random.uniform([],0,1)<=PROBABILITY, tf.float32)
        # CHOOSE RANDOM
        k = tf.cast( tf.random.uniform([],0,AUG_BATCH),tf.int32)
        a = tf.random.uniform([],0,1)*P # this is beta dist with alpha=1.0
        # MAKE MIXUP IMAGE
        img1 = image[j,]
        img2 = image[k,]
        imgs.append((1-a)*img1 + a*img2)
        # MAKE CUTMIX LABEL
        if len(label.shape)==1:
            lab1 = tf.one_hot(label[j],CLASSES)
            lab2 = tf.one_hot(label[k],CLASSES)
        else:
            lab1 = label[j,]
            lab2 = label[k,]
        labs.append((1-a)*lab1 + a*lab2)
            
    # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
    image2 = tf.reshape(tf.stack(imgs),(AUG_BATCH,DIM,DIM,N_CHANNELS))
    label2 = tf.reshape(tf.stack(labs),(AUG_BATCH,CLASSES))
    return image2,label2
"""
def transform(image,label):
    # THIS FUNCTION APPLIES BOTH CUTMIX AND MIXUP
    CLASSES = 19
    AUG_BATCH = config.BATCH_SIZE
    N_CHANNELS = 12
    SWITCH = 0.5
    CUTMIX_PROB = 0.666
    MIXUP_PROB = 0.666
    # FOR SWITCH PERCENT OF TIME WE DO CUTMIX AND (1-SWITCH) WE DO MIXUP
    image2, label2 = cutmix(image, label, CUTMIX_PROB)
    image3, label3 = mixup(image, label, MIXUP_PROB)
    imgs = []; labs = []
    for j in range(AUG_BATCH):
        P = tf.cast( tf.random.uniform([],0,1)<=SWITCH, tf.float32)
        imgs.append(P*image2[j,]+(1-P)*image3[j,])
        labs.append(P*label2[j,]+(1-P)*label3[j,])
    # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
    image4 = tf.reshape(tf.stack(imgs),(AUG_BATCH,DIM,DIM,N_CHANNELS))
    label4 = tf.reshape(tf.stack(labs),(AUG_BATCH,CLASSES))
    return image4,label4
"""
def rotate(x,y):
    """Rotation augmentation

    Args:
        x: Image

    Returns:
        Augmented image
    """

    # Rotate 0, 90, 180, 270 degrees
    return tf.image.rot90(x, tf.random.uniform(shape=[], minval=0, maxval=4, dtype=tf.int32)), y

def flip(x,y):
    """Flip augmentation

    Args:
        x: Image to flip

    Returns:
        Augmented image
    """
    x = tf.image.random_flip_left_right(x)
    x = tf.image.random_flip_up_down(x)

    return x,y

def color(x,y):
    """Color augmentation

    Args:
        x: Image

    Returns:
        Augmented image
    """
    x = tf.image.random_hue(x, 0.08)
    x = tf.image.random_saturation(x, 0.6, 1.6)
    x = tf.image.random_brightness(x, 0.05)
    x = tf.image.random_contrast(x, 0.7, 1.3)
    return x,y


def zoom(x,y):
    """Zoom augmentation

    Args:
        x: Image

    Returns:
        Augmented image
    """

    # Generate 20 crop settings, ranging from a 1% to 20% crop.
    scales = list(np.arange(0.8, 1.0, 0.01))
    boxes = np.zeros((len(scales), 4))

    for i, scale in enumerate(scales):
        x1 = y1 = 0.5 - (0.5 * scale)
        x2 = y2 = 0.5 + (0.5 * scale)
        boxes[i] = [x1, y1, x2, y2]

    def random_crop(img):
        # Create different crops for an image
        crops = tf.image.crop_and_resize([img], boxes=boxes, box_indices=np.zeros(len(scales)), crop_size=(32, 32))
        # Return a random crop
        return crops[tf.random.uniform(shape=[], minval=0, maxval=len(scales), dtype=tf.int32)]


    choice = tf.random.uniform(shape=[], minval=0., maxval=1., dtype=tf.float32)

    # Only apply cropping 50% of the time
    return tf.cond(choice < 0.5, lambda: x, lambda: random_crop(x)), y
