# some training parameters
EPOCHS = 100
BATCH_SIZE = 64
NUM_CLASSES = 19
AUGM = False
image_height = 120
image_width = 120
channels = 12
save_model_dir = "saved_model/model_hvd"
dataset_dir = "/p/scratch/joaiml/sedona3/"

train_dir = dataset_dir + "train_data/"
valid_dir = dataset_dir + "val_data/"
test_dir = "/p/project/joaiml/remote_sensing/rocco_sedona/ben_TF2/data/test.tfrecord"
